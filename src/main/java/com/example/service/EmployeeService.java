package com.example.service;

import com.example.dto.APIResponseDto;
import com.example.dto.EmployeeDto;

import java.util.List;

public interface EmployeeService {

    public EmployeeDto create(EmployeeDto dto) throws  Exception;

    public APIResponseDto getEmployeeById(long id ) throws  Exception;

    public List<EmployeeDto> getList() throws  Exception;

    public EmployeeDto updateEmployee(EmployeeDto dto,Long id) throws Exception;
    public String delete(long id) throws Exception;
}
