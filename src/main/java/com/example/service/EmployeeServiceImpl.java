package com.example.service;

import com.example.dto.APIResponseDto;
import com.example.dto.DepartmentDto;
import com.example.dto.EmployeeDto;
import com.example.model.Employee;
import com.example.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class EmployeeServiceImpl implements EmployeeService {


    private EmployeeRepository repo;
    private ModelMapper mapper;

    private RestTemplate restTemplate;

    @Value("${departmentUri}")
    private String uri;
    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repo, ModelMapper mapper,RestTemplate restTemplate) {
        this.repo = repo;
        this.mapper = mapper;
        this.restTemplate = restTemplate;
    }

    @Override
    public EmployeeDto create(EmployeeDto dto) throws Exception {
        Employee  emp = mapper.map(dto, Employee.class);
        emp= repo.save(emp);
        return mapper.map(emp,EmployeeDto.class);
    }

    @Override
    public APIResponseDto getEmployeeById(long id) throws Exception {

        Employee emp = repo.findById(id).orElseThrow(()->new RuntimeException("Employee Record is not found with "+id));
        ResponseEntity<DepartmentDto> responseEntity = restTemplate.getForEntity(uri + emp.getDepartmentCode(), DepartmentDto.class);
       DepartmentDto departmentDto = responseEntity.getBody();
        EmployeeDto dto = mapper.map(emp, EmployeeDto.class);

        APIResponseDto apiResponseDto = new APIResponseDto(dto,departmentDto);
        return apiResponseDto;
    }

    @Override
    public List<EmployeeDto> getList() throws Exception {
        List<Employee> list = repo.findAll();
        List<EmployeeDto> dtos = list.stream().map((e)->mapper
                                    .map(e,EmployeeDto.class))
                                    .collect(Collectors.toList());
        return dtos;
    }

    @Override
    public EmployeeDto updateEmployee(EmployeeDto dto, Long id) throws Exception {
        Employee emp = repo.findById(id).orElseThrow(()->new RuntimeException("Employee Record is not found with "+id));
        emp.setFirstName(dto.getFirstName());
        emp.setLastName(dto.getLastName());
        emp.setEmail(dto.getEmail());
        emp.setDepartmentCode(dto.getDepartmentCode());
        emp = repo.save(emp);
        return mapper.map(emp,EmployeeDto.class);
    }

    @Override
    public String delete(long id) throws Exception {
        Employee emp = repo.findById(id).orElseThrow(()->new RuntimeException("Employee Record is not found with "+id));
        repo.delete(emp);
        return "Record has been deleted with id : "+id;
    }
}
