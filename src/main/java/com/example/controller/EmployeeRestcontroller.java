package com.example.controller;


import com.example.dto.APIResponseDto;
import com.example.dto.EmployeeDto;
import com.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/employee")
public class EmployeeRestcontroller {

    private EmployeeService service;

    @Autowired
    public EmployeeRestcontroller(EmployeeService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<EmployeeDto> createDepartment(@RequestBody EmployeeDto dto) throws Exception {
        return new ResponseEntity<>(service.create(dto), HttpStatus.CREATED);
    }


    @GetMapping("{id}")
    public ResponseEntity<APIResponseDto> getById(@PathVariable("id") long id) throws  Exception
    {
        return  ResponseEntity.ok(service.getEmployeeById(id));
    }


    @GetMapping("/all-employee")
    public ResponseEntity<List<EmployeeDto>> getAllDepartment() throws  Exception{
        return ResponseEntity.ok(service.getList());
    }

    @PutMapping("{id}")
    public ResponseEntity<EmployeeDto> updateById(@RequestBody EmployeeDto dto,@PathVariable long id) throws Exception {
        return  ResponseEntity.ok(service.updateEmployee(dto,id));

    }


    @DeleteMapping("{id}")
    public ResponseEntity<String> deletebyId(@PathVariable("id") long id) throws  Exception
    {
        return  ResponseEntity.ok(service.delete(id));
    }
}
